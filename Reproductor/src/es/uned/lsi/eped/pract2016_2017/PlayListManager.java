package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class PlayListManager implements PlayListManagerIF {

	private ListIF<PlayListIF> gestorListaReproduccion;

	public PlayListManager() {
		gestorListaReproduccion = new List<PlayListIF>();
	}

	@Override
	public boolean contains(String playListID) {
		IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
		while (it.hasNext()) {
			PlayListIF aux = it.getNext();
			if (((PlayList) aux).getId().equals(playListID)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public PlayListIF getPlayList(String playListID) {
		PlayListIF play = new PlayList();
		IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
		while (it.hasNext()) {
			PlayListIF aux = it.getNext();
			if (((PlayList) aux).getId().equals(playListID)) {
				return aux;
			}
		}
		return play;
	}

	@Override
	public ListIF<String> getIDs() {
		ListIF<String> list = new List<String>();
		IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
		while (it.hasNext()) {
			PlayListIF aux = it.getNext();
			list.insert(((PlayList) aux).getId(), 1);
		}
		return list;
	}

	@Override
	public void createPlayList(String playListID) {
		if (this.contains(playListID) == false) {
			PlayList list = new PlayList();
			list.setId(playListID);
			gestorListaReproduccion.insert(list, 1);
		}
	}

	@Override
	public void removePlayList(String playListID) {
		int pos = 1;
		IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
		while (it.hasNext()) {
			PlayListIF aux = it.getNext();
			if ((((PlayList) aux).getId()).equals(playListID)) {
				gestorListaReproduccion.remove(pos);
			}
			pos++;
		}
	}
}
