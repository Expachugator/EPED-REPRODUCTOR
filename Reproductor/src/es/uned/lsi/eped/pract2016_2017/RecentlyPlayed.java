package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class RecentlyPlayed implements RecentlyPlayedIF {

	private ListIF<Integer> gestorPilaRecientes;

	int max;

	public RecentlyPlayed(int max) {
		this.max = max;
		gestorPilaRecientes = new List<Integer>();
	}

	@Override
	public ListIF<Integer> getContent() {
		return gestorPilaRecientes;
	}

	@Override
	public void addTune(int tuneID) {
		if (gestorPilaRecientes.size() == max) {
			gestorPilaRecientes.remove(max);
		}
		gestorPilaRecientes.insert(tuneID, 1);
	}
}
