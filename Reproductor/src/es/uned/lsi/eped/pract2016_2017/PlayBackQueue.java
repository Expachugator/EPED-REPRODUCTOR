package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Queue;
import es.uned.lsi.eped.DataStructures.QueueIF;

public class PlayBackQueue implements PlayBackQueueIF {

	private QueueIF<Integer> gestorCola;

	public PlayBackQueue() {
		gestorCola = new Queue<Integer>();
	}

	@Override
	public ListIF<Integer> getContent() {
		ListIF<Integer> listaId = new List<Integer>();
		IteratorIF<Integer> it = gestorCola.iterator();
		int pos = 1;
		while (it.hasNext()) {
			listaId.insert(it.getNext(), pos);
			pos++;
		}
		return listaId;
	}

	@Override
	public boolean isEmpty() {

		return gestorCola.isEmpty();
	}

	@Override
	public int getFirstTune() {
		return gestorCola.getFirst();
	}

	@Override
	public void extractFirstTune() {
		gestorCola.dequeue();
	}

	@Override
	public void addTunes(ListIF<Integer> lT) {
		IteratorIF<Integer> it = lT.iterator();
		while (it.hasNext()) {
			gestorCola.enqueue(it.getNext());
		}
	}

	@Override
	public void clear() {
		gestorCola.clear();
	}
}
