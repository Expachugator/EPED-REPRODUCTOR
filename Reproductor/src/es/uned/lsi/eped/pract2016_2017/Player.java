package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class Player implements PlayerIF {

	private TuneCollection tuneCollection;
	private PlayListManager playListManager;
	private PlayBackQueue playBackQueue;
	private RecentlyPlayed recentlyPlayed;

	public Player(TuneCollection tCollection, int maxRecentlyPlayed) {
		tuneCollection = tCollection;
		playListManager = new PlayListManager();
		playBackQueue = new PlayBackQueue();
		recentlyPlayed = new RecentlyPlayed(maxRecentlyPlayed);
	}

    @Override
	public void addListOfTunesToPlayBackQueue(ListIF<Integer> L) {
		playBackQueue.addTunes(L);
	}

    @Override
	public void addListOfTunesToPlayList(String playListID, ListIF<Integer> L) {
		playListManager.getPlayList(playListID).addListOfTunes(L);
	}

    @Override
	public void addPlayListToPlayBackQueue(String playListID) {
		PlayListIF playList = playListManager.getPlayList(playListID);
		if (playList != null) {
			ListIF<Integer> lista = playList.getPlayList();
			playBackQueue.addTunes(lista);
		}
	}

    @Override
	public void addSearchToPlayBackQueue(String title, String author, String genre, String album, int min_year,
			int max_year, int min_duration, int max_duration) {
		ListIF<Integer> list = new List<Integer>();
		Query query = new Query(title, author, genre, album, min_year, max_year, min_duration, max_duration);
		int pos = 1;
		for (int i = 0; i < tuneCollection.size(); i++) {
			Tune tune = tuneCollection.getTune(i);
			if (tune.match(query)) {
				list.insert(i, pos);
				pos++;
			}
		}
		playBackQueue.addTunes(list);
	}

    @Override
	public void addSearchToPlayList(String playListID, String title, String author, String genre, String album,
			int min_year, int max_year, int min_duration, int max_duration) {
		ListIF<Integer> list = new List<Integer>();
		Query query = new Query(title, author, genre, album, min_year, max_year, min_duration, max_duration);
		PlayListIF playList;
		if (playListManager.contains(playListID)) {
			playList = playListManager.getPlayList(playListID);
			int pos = 1;
			for (int i = 0; i < tuneCollection.size(); i++) {
				Tune tune = tuneCollection.getTune(i);
				if (tune.match(query) == true) {
					list.insert(i, pos);
					pos++;
				}
			}
			playList.addListOfTunes(list);
		}
	}

    @Override
	public void clearPlayBackQueue() {
		playBackQueue.clear();
	}

    @Override
	public void createPlayList(String playListID) {
		playListManager.createPlayList(playListID);
	}

    @Override
	public void play() {
		if (playBackQueue.isEmpty() == false) {
			recentlyPlayed.addTune(playBackQueue.getFirstTune());
			playBackQueue.extractFirstTune();
		}
	}

    @Override
	public void removePlayList(String playListID) {
		playListManager.removePlayList(playListID);
	}

    @Override
	public void removeTuneFromPlayList(String playListID, int tuneID) {
		playListManager.getPlayList(playListID).removeTune(tuneID);
	}

    @Override
	public ListIF<Integer> getPlayBackQueue() {
		return playBackQueue.getContent();
	}

    @Override
	public ListIF<Integer> getPlayListContent(String playListID) {
		return playListManager.getPlayList(playListID).getPlayList();
	}

    @Override
	public ListIF<String> getPlayListIDs() {
		return playListManager.getIDs();
	}

    @Override
	public ListIF<Integer> getRecentlyPlayed() {
	    return recentlyPlayed.getContent();
	}

}