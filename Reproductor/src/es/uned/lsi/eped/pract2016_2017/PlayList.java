package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class PlayList implements PlayListIF {

	private ListIF<Integer> gestorLista;
	private String id;

	public PlayList() {
		gestorLista = new List<Integer>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    @Override
	public ListIF<Integer> getPlayList() {
		return gestorLista;
	}

    @Override
	public void addListOfTunes(ListIF<Integer> lT) {
		IteratorIF<Integer> it = lT.iterator();
		while (it.hasNext()) {
			Integer guardar = it.getNext();
			Integer posicion = this.getPlayList().size() + 1;
			gestorLista.insert(guardar, posicion);
		}
	}

	@Override
	public void removeTune(int tuneID) {
		IteratorIF<Integer> it = gestorLista.iterator();
		int pos = 1;
		while (it.hasNext()) {
			if (tuneID == it.getNext()) {
				gestorLista.remove(pos);
				pos--;
			}
			pos++;
		}
	}
}
